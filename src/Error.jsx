import React, { Component } from 'react';

class Error extends Component {
    render() { 
        return ( 
            <section className="error-msg">
                
                <h1 className="">Error: Route does not exist</h1>

            </section>
         );
    }
}
 
export default Error;