import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import './icons.css';
import Login from './Components/login';
import Dashboard from "./Components/dashboard";
import Merchant from "./Components/merchant";
import CreateMerchant from "./Components/create-merchant";
import Admin from "./Components/admin";
import Users from "./Components/users";
import Portal from "./Components/portal";
import Redemption from "./Components/redemption";
import Settings from "./Components/settings";
import Error from './Error';


class App extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  loginHandle = () => {
    this.setState({loggedIn: true})
  }
  render() {
    return (
      <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route path="/dashboard" component={Dashboard}/>
          <Route exact path="/merchant" component={Merchant}/>
          <Route exact path="/merchant/create" component={CreateMerchant}/>
          <Route path="/settings" component={Settings}/>
          <Route path="/portal" component={Portal}/>
          <Route path="/admin" component={Admin}/>
          <Route path="/users" component={Users}/>
          <Route path="/redemption" component={Redemption}/>
          <Route component={Error}/>
        </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
