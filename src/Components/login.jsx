import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import logo from '../images/logo.png';

class Login extends Component {
    render() { 
        return ( 
            <section className="login-bg">
                <div className="login-card">
                    <h2>
                        <span>Sign In</span>
                        <img src={logo} alt="red logo" />
                    </h2>
                    <form>
                        <div className="ubaapp-form-group">
                            <label for="email">Email</label>
                            <input type="text" id="email" />
                        </div>
                        <div className="ubaapp-form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" />
                        </div>
                        <a><small>Forgot Your Password?</small></a>
                        <div>
                            <NavLink to="/dashboard">
                                <button className="ubaapp-button login-button">
                                    <span>Login</span><span>&#62;</span>
                                </button>
                            </NavLink>
                        </div>
                    </form>
                    <p>Don't have an account?</p>
                    <div>
                        <a>
                            <button className="ubaapp-button register-button">
                                <span>Register</span><span>&#62;</span>
                            </button>
                        </a>
                    </div>
                </div>

            </section>
         );
    }
}
 
export default Login;