import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";
import { NavLink } from "react-router-dom";

class Merchant extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <Header handleClick={this.handleToggleClick} />
          <div className="dashboard-body">
            <Sidebar show={this.state.sideBarOpen} />
            <main className="main-content admin">
              <div className="no-merchant">
                There are no merchant partners yet.
                <NavLink to="/merchant/create">Create one</NavLink>
              </div>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Merchant;
