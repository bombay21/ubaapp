import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";
import AdminData from "../admindata.json";
import Dropdown from "./dropdown";

class Admin extends Component {
  state = { sideBarOpen: false, loggedIn: false };
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <Header handleClick={this.handleToggleClick} />
          <div className="dashboard-body">
            <Sidebar show={this.state.sideBarOpen} />
            <main className="main-content admin">
              <div className="main-content--top">
                <div className="time-section">
                  <span className="caption">Admin Users</span>
                  <Dropdown
                    items={[
                      { value: "Today", id: 1 },
                      { value: "Yesterday", id: 2 },
                      { value: "Last 7 Days", id: 3 },
                      { value: "Last 30 Days", id: 4 },
                      { value: "This Month", id: 5 },
                      { value: "Last Month", id: 6 },
                      { value: "Custom Range", id: 7 }
                    ]}
                  />
                </div>
                <div className="admin-section">
                  <button>New Admin User</button>
                  <form>
                    <div className="ubaapp-form-group search-form">
                      <input type="text" id="search" placeholder="Search" />
                      <i className="">&larr;</i>
                    </div>
                  </form>
                </div>
              </div>
              <div className="main-content--bottom table-settings">
                <table>
                  <tr>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Current Sign In At</th>
                    <th>Last Sign In At</th>
                    <th>Status</th>
                    <th />
                  </tr>

                  {AdminData.map((adminDetail, index) => {
                    return (
                      <tr>
                        <td>{adminDetail.email}</td>
                        <td>{adminDetail.role}</td>
                        <td>{adminDetail.currentSignIn}</td>
                        <td>{adminDetail.lastSignIn}</td>
                        <td>
                          <span
                            className={
                              adminDetail.isActive
                                ? "status-indicator-green"
                                : "status-indicator-orange"
                            }
                          />
                          {adminDetail.isActive ? "Active" : "Inactive"}
                        </td>
                        <td>
                          <span>&#8230;</span>
                          &nbsp;&nbsp;
                        </td>
                      </tr>
                    );
                  })}
                </table>
              </div>
              <div className="pagination">
                <div className="notes">
                  Displaying Users 1 - 39 of 39 in total
                </div>
                <div className="page-range">
                  <div className="direction">&larr;</div>
                  <div>Page 1 of 2</div>
                  <div className="direction">&rarr;</div>
                </div>
                <div className="download">
                  Download: <span>CSV XML JSON</span>
                </div>
              </div>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Admin;
