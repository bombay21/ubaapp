import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";
import UserData from "../userdata.json";
import Dropdown from "./dropdown";

class Users extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
        <Header handleClick={this.handleToggleClick} />
        <div className="dashboard-body">
        <Sidebar show={this.state.sideBarOpen} />
        <main className="main-content admin">
        <div className="main-content--top">
          <div className="time-section">
            <span className="caption">Users</span>
            <Dropdown
              items={[
                { value: "Today", id: 1 },
                { value: "Yesterday", id: 2 },
                { value: "Last 7 Days", id: 3 },
                { value: "Last 30 Days", id: 4 },
                { value: "This Month", id: 5 },
                { value: "Last Month", id: 6 },
                { value: "Custom Range", id: 7 }
              ]}
            />
          </div>
          <form>
            <div className="ubaapp-form-group search-form">
              <input type="text" id="search" placeholder="Search" />
              <i className="">&larr;</i>
            </div>
          </form>
        </div>
        <div className="main-content--bottom table-settings">
          <table>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Joined</th>
              <th />
            </tr>
            {UserData.map((userDetail, index) => {
              return (
                <tr>
                  <td>{userDetail.id}</td>
                  <td>{userDetail.firstname}</td>
                  <td>{userDetail.lastname}</td>
                  <td>{userDetail.email}</td>
                  <td>{userDetail.phone}</td>
                  <td>{userDetail.joined}</td>
                  <td>
                    <span>&#8230;</span>
                    &nbsp;&nbsp;
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
        </main>
        </div>
      </div>
      </React.Fragment>
    );
  }
}

export default Users;
