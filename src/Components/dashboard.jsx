import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";
import Dropdown from "./dropdown";
import bluegraph from "../images/svg/graph-bl.svg";
import greengraph from "../images/svg/graph-gr.svg";
import orangegraph from "../images/svg/graph-o.svg";
import yellowgraph from "../images/svg/graph-y.svg";
import allgraph from "../images/svg/graph-all.svg";

class Dashboard extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <Header handleClick={this.handleToggleClick} />
          <div className="dashboard-body">
            <Sidebar show={this.state.sideBarOpen} />
            <main className="main-content admin">
              <div className="main-content--top">
                <div className="time-section">
                  <span className="caption">Dashboard</span>
                  <Dropdown
                    items={[
                      { value: "Today", id: 1 },
                      { value: "Yesterday", id: 2 },
                      { value: "Last 7 Days", id: 3 },
                      { value: "Last 30 Days", id: 4 },
                      { value: "This Month", id: 5 },
                      { value: "Last Month", id: 6 },
                      { value: "Custom Range", id: 7 }
                    ]}
                  />
                </div>
                <form>
                  <div className="ubaapp-form-group search-form">
                    <input type="text" id="search" placeholder="Search" />
                    <i className="">&larr;</i>
                  </div>
                </form>
              </div>
              <div className="main-content--bottom">
                <div className="display-boxes">
                  <div className="display-box box1">
                    <p className="total">1,234</p>
                    <p className="block-description">Registered Customers</p>
                    <div className="plot">
                      <img src={bluegraph} alt="" />
                    </div>
                  </div>
                  <div className="display-box box2">
                    <p className="total">3,456</p>
                    <p className="block-description">Value of Redemptions</p>
                    <div className="plot">
                      <img src={orangegraph} alt="" />
                    </div>
                  </div>
                  <div className="display-box box3">
                    <p className="total">1,234</p>
                    <p className="block-description">Points Redeemed</p>
                    <div className="plot">
                      <img src={greengraph} alt="" />
                    </div>
                  </div>
                  <div className="display-box box4">
                    <p className="total">1,234,987</p>
                    <p className="block-description">Cash Spend (Debit Card)</p>
                    <div className="plot">
                      <img src={yellowgraph} alt="" />
                    </div>
                  </div>
                </div>
                <div className="summary-box">
                  <div className="summary-box--top">
                    <div className="caption">Summary</div>
                    <div className="caption">
                      <span>Month</span>
                      <span className="time-class">All time</span>
                    </div>
                  </div>
                  <div className="summary-box--bottom">
                    <div className="four-parts">
                      <div className="part part1">
                        <p className="info">Lorem Ipsum</p>
                        <p className="bold-info">123456789</p>
                      </div>
                      <div className="part part2">
                        <p className="info">Lorem Ipsum</p>
                        <p className="bold-info">123456789</p>
                      </div>
                      <div className="part part3">
                        <p className="info">Lorem Ipsum</p>
                        <p className="bold-info">123456789</p>
                      </div>
                      <div className="part part4">
                        <p className="info">Lorem Ipsum</p>
                        <p className="bold-info">123456789</p>
                      </div>
                    </div>
                    <div className="summary-plot">
                      <img src={allgraph} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;
