import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";
import Dropdown from "./dropdown";

class Redemption extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
        <Header handleClick={this.handleToggleClick} />
        <div className="dashboard-body">
        <Sidebar show={this.state.sideBarOpen} />
        <main className="main-content admin">
        <div class="main-content--top">
          <div class="time-section">
            <span class="caption">Redemption Report</span>
            <Dropdown
              items={[
                { value: "Today", id: 1 },
                { value: "Yesterday", id: 2 },
                { value: "Last 7 Days", id: 3 },
                { value: "Last 30 Days", id: 4 },
                { value: "This Month", id: 5 },
                { value: "Last Month", id: 6 },
                { value: "Custom Range", id: 7 }
              ]}
            />
          </div>
          <form>
            <div class="ubaapp-form-group search-form">
              <input type="text" id="search" placeholder="Search" />
              <i class="">&larr;</i>
            </div>
          </form>
        </div>
        <div class="main-content--bottom table-settings">
          <table>
            <tr>
              <th>Product</th>
              <th>Value (Points)</th>
              <th>Value (Naira)</th>
              <th>Portal Partner</th>
              <th>Satus</th>
              <th>Created At</th>
              <th />
            </tr>
            <tr>
              <td>Airtime</td>
              <td>500</td>
              <td>100</td>
              <td>Interswitch</td>
              <td>Incomplete</td>
              <td>26 Oct 2017</td>
              <td>
                <span>&#8230;</span>
                &nbsp;&nbsp;
              </td>
            </tr>
            <tr>
              <td>Airtime</td>
              <td>500</td>
              <td>100</td>
              <td>Interswitch</td>
              <td>Incomplete</td>
              <td>26 Oct 2017</td>
              <td>
                <span>&#8230;</span>
                &nbsp;&nbsp;
              </td>
            </tr>
            <tr>
              <td>Airtime</td>
              <td>500</td>
              <td>100</td>
              <td>Interswitch</td>
              <td>Incomplete</td>
              <td>26 Oct 2017</td>
              <td>
                <span>&#8230;</span>
                &nbsp;&nbsp;
              </td>
            </tr>
          </table>
        </div>
        </main>
        </div>
      </div>
      </React.Fragment>
    );
  }
}

export default Redemption;
