import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";

class CreateMerchant extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <Header handleClick={this.handleToggleClick} />
          <div className="dashboard-body">
            <Sidebar show={this.state.sideBarOpen} />
            <main className="main-content admin">
              <div className="main-content--top merchant-header">
                <div className="time-section">
                  <span className="caption">Merchant Partners</span>
                </div>
              </div>
              <div className="main-content--bottom">
                <div className="merchant-form">
                  <form>
                    <div className="merchant-form-fields">
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Name *</label>
                        <input type="text" id="name" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Address *</label>
                        <input type="text" id="address" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Email</label>
                        <input type="text" id="email" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Phone *</label>
                        <input type="text" id="phone" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Additional Info</label>
                        <input type="text" id="additional-info" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">
                          image (size: 400 X 400 pixels) *
                        </label>
                        <input type="text" id="image-upload" />
                      </div>
                      <div className="ubaapp-form-group merchant-group">
                        <label htmlFor="email">Website</label>
                        <input type="text" id="email" />
                      </div>
                      <div className="ubaapp-form-group merchant-group checkboxes">
                        <div className="align-checkboxes">
                          <input type="checkbox" id="active-checkbox" />
                          <label htmlFor="active-checkbox">Active</label>
                        </div>
                        <div className="align-checkboxes">
                          <input type="checkbox" id="featured-checkbox" />
                          <label htmlFor="featured-checkbox">Featured</label>
                        </div>
                        <div className="align-checkboxes">
                          <input type="checkbox" id="delivery-checkbox" />
                          <label htmlFor="delivery-checkbox">
                            Does Home Delivery
                          </label>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div className="create-merchant-container">
                    <button>Create Discount Partner</button>
                    <button className="button-cancel">Cancel</button>
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CreateMerchant;
