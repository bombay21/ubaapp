import React, { Component } from "react";
import Header from "./Navigation/Header";
import Sidebar from "./Navigation/Sidebar";

class Settings extends Component {
  state = { sideBarOpen: false, loggedIn: false};
  handleToggleClick = () => {
    this.setState(prevState => {
      return { sideBarOpen: !prevState.sideBarOpen };
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <Header handleClick={this.handleToggleClick} />
          <div className="dashboard-body">
            <Sidebar show={this.state.sideBarOpen} />
            <main className="main-content admin">
              <div className="main-content--top merchant-header">
                <div className="time-section">
                  <span className="caption">System Settings</span>
                </div>
              </div>
              <div className="main-content--bottom table-settings">
                <table>
                  <tr>
                    <th />
                    <th>First Points Conversion Rate</th>
                    <th>Max Bank Accounts Per User</th>
                    <th>Max UBA Policies Per User</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th />
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>0.2</td>
                    <td>3</td>
                    <td>1</td>
                    <td>21 Oct 2017</td>
                    <td>26 Oct 2017</td>
                    <td>
                      <span>&larr;</span>
                      &nbsp;&nbsp;
                    </td>
                  </tr>
                </table>
              </div>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Settings;
