import React, { Component } from "react";

class Dropdown extends Component {
  state = {
    items: this.props.items || [],
    showItems: false,
    selectedItem: this.props.items && this.props.items[0]
  };

  dropDown = () => {
      this.setState(prevState => ({
          showItems: !prevState.showItems
      }))
  }

  selectedItem = (item) => this.setState({
      selectedItem: item,
      showItems: false
  })

  render() {
    return (
      <React.Fragment>
        <div className="date-selector">
          <span className="period">
          {this.state.selectedItem.value}:
          </span>
          <span className="date"></span>
          <div className="icon-stack"
          onClick={this.dropDown} 
          >
            <span className="caret-down">&#62;</span>
          </div>
        </div>
        <div
          className="dropdown"
          style={{ display: this.state.showItems ? "block" : "none" }}
        >
          <ul>
            {this.state.items.map(item => (
              <li 
              key={item.id}
              onClick={() => this.selectedItem(item)}
              className={this.state.selectedItem === item ? 'selected' : ''}
              >
              {item.value}
              </li>
            ))}
            <li>
              <button className="apply-button">Apply</button>
              <button className="cancel-button"
              onClick={() => this.state.showItems = !this.showItems}
              >Cancel</button>
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default Dropdown;
