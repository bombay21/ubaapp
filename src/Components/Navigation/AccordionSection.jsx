import React, { Component } from "react";

class AccordionSection extends Component {
  
  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const {
      onClick,
      props: { isOpen, label }
    } = this;

    return (
      <React.Fragment>
        <div className="panel">
          <div onClick={onClick} style={{ cursor: "pointer" }}>
            {label}
            <div className="caret" style={{ position: "absolute", right: "0", top: "58%" }}>
              {!isOpen && <span>&#62;</span>}
              {isOpen && <span>v</span>}
            </div>
          </div>
        </div>
          {isOpen && (
            <div style={{ padding: "0px 0 0 40px", color: "#FFFFFF"}}>
              {this.props.children}
            </div>
          )}
      </React.Fragment>
    );
  }
}

export default AccordionSection;
