import React from 'react';

const Hamburger = props => (
    <div onClick={props.click} className="drawer">
        <div id="bar1" className="bar rot45" />
        <div id="bar2" className="bar disappear" />
        <div id="bar3" className="bar rot-45" />
    </div>
);

export default Hamburger;