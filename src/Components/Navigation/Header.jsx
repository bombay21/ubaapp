import React from "react";
import logo from "../../images/rewards-logo.png";
import profile from "../../images/panda-head.jpg";
import Hamburger from "./Hamburger";

const Header = props => (
  <header>
    <a className="logo">
      <img src={logo} alt="Rewards Logo" />
    </a>
    <ul>
      <li>
        <div className="notification">
          <span className="">&#968;</span>
        </div>
      </li>
      <li>
        <img className="profile-image" src={profile} alt="" />
      </li>
      <li>
        <Hamburger click={props.handleClick} />
      </li>
    </ul>
  </header>
);

export default Header;
