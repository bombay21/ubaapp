import React from "react";
import logo from "../../images/logo-red.png";
import Accordion from "./Accordion";
import { NavLink } from "react-router-dom";

const Sidebar = props => {
  let drawerClasses = "side-drawer top-space";
  if (props.show) {
    drawerClasses = "side-drawer top-space open";
  }
  return (
    <aside className={drawerClasses}>
      <div>
        <NavLink to="/dashboard">
          <div className="outer-panel dash" tabIndex="-1" role="tabpanel">
            Dashboard
          </div>
        </NavLink>
        <Accordion>
          <div label="Content Management" role="tabpanel">
            <NavLink to="/merchant">
              <p>Merchant Partners</p>
            </NavLink>
          </div>
          <div label="User Management" role="tabpanel">
            <NavLink to="/admin">
              <p>Admin Users</p>
            </NavLink>
            <NavLink to="/users">
              <p>Users</p>
            </NavLink>
          </div>
          <div label="Reporting" role="tabpanel">
            <NavLink to="/portal">
              <p>Portal</p>
            </NavLink>
            <NavLink to="/redemption">
              <p>Redemption Report</p>
            </NavLink>
            <NavLink to="/BI">
              <p>BI</p>
            </NavLink>
          </div>
        </Accordion>
        <NavLink to="/settings">
          <div className="outer-panel sett" tabIndex="0" role="tabpanel">
            System Settings
          </div>
        </NavLink>
      </div>
      <footer>
        <img src={logo} alt="UBA Red Logo" />
        <p>Copyright &copy; 2018. UBA Group PLC</p>
      </footer>
    </aside>
  );
};

export default Sidebar;
