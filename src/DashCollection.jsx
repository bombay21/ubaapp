import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from "./Components/Navigation/Header";
import Sidebar from "./Components/Navigation/Sidebar";
import Dashboard from "./Components/dashboard";
import Merchant from "./Components/merchant";
// import Merchant from "./Components/merchant/";
import Admin from "./Components/admin";
import Users from "./Components/users";
import Portal from "./Components/portal";
import Redemption from "./Components/redemption";
import Settings from "./Components/settings";

class DashCollection extends Component {
  state = {sideBarOpen: false};
  handleToggleClick = () => {
  this.setState((prevState) => {
    return {sideBarOpen: !prevState.sideBarOpen};
  });
 };
  render() {
    
    return (
        <BrowserRouter>
        <div className="dashboard-container">
        <Header handleClick={this.handleToggleClick} />
        <div className="dashboard-body">
        <Sidebar show={this.state.sideBarOpen} />
        <main className="main-content admin">
        <Switch>
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/merchant" component={Merchant}/>
          <Route path="/settings" component={Settings}/>
          <Route path="/portal" component={Portal}/>
          <Route path="/admin" component={Admin}/>
          <Route path="/users" component={Users}/>
          <Route path="/redemption" component={Redemption}/>
        </Switch>
        </main>
        </div>
      </div>
      </BrowserRouter>
    );
  }
}

export default DashCollection;
